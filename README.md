# German Radiology Report Labeler

## Description
This repository contains a specialized tool for automatically extracting annotations from German thoracic radiology
reports. The underlying methodology and logic are comprehensively detailed in our accompanying research paper, which
can be accessed [here](https://arxiv.org/pdf/2306.02777).

## Installation
To install the necessary dependencies for this project, use the following command: `poetry install`


## Usage
To utilize this tool, you should first configure the file paths in `code/main.py`. Detailed comments are provided
within the file to guide you in understanding the purpose of each path setting. 

Upon executing the `main.py` script, the algorithm processes the `input.csv` file, identifying and labeling relevant
medical observations. The results, including labeled data and corresponding report texts, are then saved to the
`output.csv` file as specified in your path configurations.

## Authors and Acknowledgment
This project is a collaborative effort by:

- Alessandro Wollek
- Philip Haitzer
- Thomas Sedlmeyr


## License
For open source projects, say how it is licensed.

