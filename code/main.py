from negex import sortRules

from simple_german_chexpert import load_observation_phrases, analyse_csv_file_multi_threading

if __name__ == "__main__":
    # The File containing the radiology reports. This file should contain two columns:
    # report_index (a unique identifier for each report) and "report_text" (the text of the report).
    path_input_file = "../labeling_directory/input.csv"

    # The location of the file where labels and the corresponding radiology report will be saved
    path_output_file = "../labeling_directory/output.csv"

    # Set the directory path where the phrase lists are located. These lists include phrases
    # categorized as negative, positive, and uncertain for various observations in the reports.
    path_to_phrases = "../phrases_and_triggers/phrases/"

    # The file containing the negex-triggers used for
    path_negex_triggers = "../phrases_and_triggers/german_negex_triggers.txt"

    # The observations which the algorithm should find in the radiology reports
    observations = ["atelectasis", "cardiomegaly", "consolidation", "edema", "enlarged_cardiomediastinum",
                    "fracture", "lung_lesion", "lung_opacity", "no_finding", "pleural_effusion", "pleural_other",
                    "pneumonia", "pneumothorax", "support_devices"]

    with open(path_negex_triggers, "r") as rfile:
        rules = sortRules(rfile.readlines())

    observation_phrases_dict = load_observation_phrases(path_to_phrases, observations)
    analyse_csv_file_multi_threading(path_input_file, path_output_file, rules, num_threads=12,
                                     phrases=observation_phrases_dict, write=True)
