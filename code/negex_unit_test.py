import unittest
from negex import sortRules
from negex import negTagger

def preprocess(input_text):
    result = input_text.lower()
    result = result.replace("ä", "ae")
    result = result.replace("ö", "oe")
    result = result.replace("ü", "ue")
    return result


class TestNegex(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        with open(r'../phrases_and_triggers/german_negex_triggers.txt') as f:
            self.rules = sortRules(f.readlines())

        with open(r'../labeling_directory/input.csv') as f:
            self.test_input = f.readlines()

    def test_mention_extraction_positive(self):
        sentence = self.test_input[1][2:]
        phrases = (["Kardiomegalie"], [], [])
        tagger = negTagger(sentence=preprocess(sentence), phrases=phrases, rules=self.rules)
        label = tagger.getNegationFlag()
        self.assertEqual(label, "1")

    def test_negation_detection(self):
        sentence = self.test_input[2][2:]
        phrases = (["Pneumothorax"], [], [])
        tagger = negTagger(sentence=preprocess(sentence), phrases=phrases, rules=self.rules)
        label = tagger.getNegationFlag()
        self.assertEqual(label, "0")

    def test_uncertainty_detection(self):
        sentence = self.test_input[3][2:]
        phrases = (["Infiltrat"], [], [])
        tagger = negTagger(sentence=preprocess(sentence), phrases=phrases, rules=self.rules)
        label = tagger.getNegationFlag()
        self.assertEqual(label, "-1")
