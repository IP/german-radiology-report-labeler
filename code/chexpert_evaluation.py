import sqlite3

import pandas as pd
from sklearn import metrics
from sklearn.metrics import confusion_matrix


def annotation_to_chexpert(df):
    """Convert the labels from the web interface to CheXpert labels.
        Positive 1 -> '1'
        Negative: -1 -> '0'
        Uncertain: 2 -> '-1'
        None: 0 -> ''
    """
    if not '' in df["pneumonia"].unique():  # arbitrary choice
        return df.replace(1, '1').replace(-1, '0').replace(2, '-1').replace(0, '')
    return df


def count_false_positives_and_negatives(df, kind="found"):
    """Count false positives and negatives returned from get_y_true_and_y_pred result."""
    if kind == "found":
        df_false_negatives = df.loc[(df.y_true_found == 1) & (df.y_pred_found == 0)]
        df_false_positives = df.loc[(df.y_true_found == 0) & (df.y_pred_found == 1)]
    elif kind == "negative":
        df_false_negatives = df.loc[(df.y_true_negative == 1) & (df.y_pred_negative == 0)]
        df_false_positives = df.loc[(df.y_true_negative == 0) & (df.y_pred_negative == 1)]
    elif kind == "uncertain":
        df_false_negatives = df.loc[(df.y_true_uncertain == 1) & (df.y_pred_uncertain == 0)]
        df_false_positives = df.loc[(df.y_true_uncertain == 0) & (df.y_pred_uncertain == 1)]
    else:
        raise ValueError(f"Unknown kind {kind}.")
    print("False Negatives", len(df_false_negatives))
    print("False Positives", len(df_false_positives))
    return df_false_negatives, df_false_positives


def add_phrase(phrase, kind, phrases, observation):
    kind_to_index = {"positive": 0, "negative": 1, "uncertain": 2}
    if phrase not in phrases[observation][kind_to_index[kind]]:
        phrases[observation][kind_to_index[kind]].append(phrase)


def parse_input_file(file_path):
    df = pd.read_csv(file_path)
    target_pneumo = []
    pred_pneumo = []
    for index, row in df.iterrows():
        if row["PNEU LINKS"] == "Kein Pneu" and row["PNEU RECHTS"] == "Kein Pneu":
            target_pneumo.append(0)
        else:
            target_pneumo.append(1)
        if row["pneumothorax"] == 1 or row["pneumothorax"] == -1:
            pred_pneumo.append(1)
        else:
            pred_pneumo.append(0)

    return target_pneumo, pred_pneumo, df


def print_metrics(y_true, y_pred):
    cm = confusion_matrix(y_true, y_pred)
    tn = cm[0][0]
    fn = cm[1][0]
    tp = cm[1][1]
    fp = cm[0][1]

    print("sensitivity: ", tp / (tp + fn))
    print("specificity: ", tn / (tn + fp))
    print("f1-score:    ", metrics.f1_score(y_true, y_pred))
    print("recall:      ", metrics.recall_score(y_true, y_pred))
    print("precision:   ", metrics.precision_score(y_true, y_pred))


def calculate_table_metric(y_true, y_pred):
    precision = "N/A"
    recall = "N/A"
    f1 = "N/A"

    if sum(y_true) != 0:
        precision = round(metrics.precision_score(y_true, y_pred), 3)

    if sum(y_true) != 0:
        recall = round(metrics.recall_score(y_true, y_pred), 3)

    if precision != "N/A" and recall != "N/A":
        if precision + recall != 0:
            f1 = round(metrics.f1_score(y_true, y_pred), 3)

    result_dict = {'f1': f1,
                   'recall': recall,
                   'precision': precision}

    return result_dict


def extract_wrong_predictions(labeled_report_path, print_fp, print_fn):
    target_pneumo, pred_pneumo, df = parse_input_file(labeled_report_path)
    df['ReportTextDiagnosis'] = df['ReportTextDiagnosis'].astype("string")

    false_positive_indices = []
    false_negative_indices = []
    for i in range(len(target_pneumo)):
        if target_pneumo[i] == 0 and pred_pneumo[i] == 1:
            false_positive_indices.append(i)
        elif target_pneumo[i] == 1 and pred_pneumo[i] == 0:
            false_negative_indices.append(i)

    if print_fp:
        print("False positive")
        print_debug_output(false_positive_indices, df)

    if print_fn:
        print("False negative")
        print_debug_output(false_negative_indices, df)


def print_wrong_predictions(predictions, y_true, report_texts, print_fp, print_fn, report_indices):
    if len(predictions) != len(y_true):
        print("The predictions and y_true have different sizes!")

    false_positive_indices = []
    false_negative_indices = []
    for i in range(len(predictions)):
        if y_true[i] == 0 and predictions[i] == 1:
            false_positive_indices.append(report_indices[i])
        elif y_true[i] == 1 and predictions[i] == 0:
            false_negative_indices.append(report_indices[i])

    if print_fp:
        print("False positive")
        print_debug_output_2(false_positive_indices, report_texts, predictions, y_true)

    if print_fn:
        print("False negative")
        print_debug_output_2(false_negative_indices, report_texts, predictions, y_true)
    return {"fp_idx": false_positive_indices, "fn_idx": false_negative_indices}


def print_debug_output_2(wrong_indices, report_texts, predictions, y_true):
    for index in wrong_indices:
        text = report_texts[index]
        print("------------------------------------")
        print("index: " + str(index))
        print(text)
        print("prediction: " + str(predictions[index]))
        true_label = true_df.loc[true_df['report_index'] == index, 'enlarged_cardiomediastinum'].item()
        print("true(different format): " + str(true_label))
        print("------------------------------------")


def print_debug_output(row_indecis, df):
    for index in row_indecis:
        text = df.iloc[index]['ReportTextDiagnosis']
        text += df.iloc[index]['ReportTextReport']
        print("------------------------------------")
        print("index: " + str(index))
        print(text)
        print("prediction: " + str(df.iloc[index]["pneumothorax"]))
        print("------------------------------------")
        if index > 1000:
            break


def get_y_pred_and_y_true(observation, automatic_labeler_data, ground_truth_data):
    ground_truth = ground_truth_data[[observation, "id"]]
    automatic_labeler = automatic_labeler_data[[observation, "id", "report_text"]]
    ground_truth = ground_truth.rename(columns={observation: 'y_true'})
    automatic_labeler = automatic_labeler.rename(columns={observation: 'y_pred'})
    df = pd.merge(ground_truth, automatic_labeler, on="id")

    df["y_true_found"] = df.y_true.apply(lambda x: 0 if x == '' else 1)
    df["y_pred_found"] = df.y_pred.apply(lambda x: 0 if x == '' else 1)

    df["y_true_negative"] = df.apply(lambda row: -1 if row.y_true_found == 0 else (1 if row.y_true == '0' else 0),
                                     axis=1)
    df["y_pred_negative"] = df.apply(lambda row: -1 if row.y_true_found == 0 else (1 if row.y_pred == '0' else 0),
                                     axis=1)

    df["y_true_uncertain"] = df.apply(lambda row: -1 if row.y_true_found == 0 else (1 if row.y_true == '-1' else 0),
                                      axis=1)
    df["y_pred_uncertain"] = df.apply(lambda row: -1 if row.y_true_found == 0 else (1 if row.y_pred == '-1' else 0),
                                      axis=1)

    return df


def add_metric_of_one_observation(observation, automatic_labeler_data, ground_truth_data, result_df):
    df = get_y_pred_and_y_true(observation, automatic_labeler_data, ground_truth_data)
    mention_metrics = calculate_table_metric(df.y_true_found, df.y_pred_found)
    negation_metrics = calculate_table_metric(df.y_true_negative.loc[df.y_true_negative != -1],
                                              df.y_pred_negative.loc[df.y_pred_negative != -1])
    uncertain_metrics = calculate_table_metric(df.y_true_uncertain.loc[df.y_true_uncertain != -1],
                                               df.y_pred_uncertain.loc[df.y_pred_uncertain != -1])

    new_row = {'Findings': observation,
               'Mention-F1': mention_metrics['f1'], 'Mention-Recall': mention_metrics['recall'],
               'Mention-Precision': mention_metrics['precision'],
               'Negation-F1': negation_metrics['f1'], 'Negation-Recall': negation_metrics['recall'],
               'Negation-Precision': negation_metrics['precision'],
               'Uncertainty-F1': uncertain_metrics['f1'], 'Uncertainty-Recall': uncertain_metrics['recall'],
               'Uncertainty-Precision': uncertain_metrics['precision'],
               }

    result_df.loc[len(result_df)] = new_row


def create_evaluation_table(pred_df, true_df, target_path="evaluation_table.csv"):
    mention_names = ["atelectasis", "cardiomegaly", "consolidation", "edema", "enlarged_cardiomediastinum",
                     "fracture", "lung_lesion", "lung_opacity", "no_finding", "pleural_effusion", "pleural_other",
                     "pneumonia", "pneumothorax", "support_devices"]

    column_names = ['Findings', 'Mention-F1', 'Mention-Recall', 'Mention-Precision', 'Negation-F1', 'Negation-Recall',
                    'Negation-Precision',
                    'Uncertainty-F1', 'Uncertainty-Recall', 'Uncertainty-Precision']
    table_df = pd.DataFrame(columns=column_names)

    for name in mention_names:
        add_metric_of_one_observation(name, pred_df, true_df, table_df)

    table_df.to_csv(target_path, index=False)


if __name__ == "__main__":
    path_ground_truth_data = ""
    path_automatic_labeler_data = ""

    pred_df = pd.read_csv(path_automatic_labeler_data)
    true_df = pd.read_csv(path_ground_truth_data)

    create_evaluation_table(pred_df, true_df)
