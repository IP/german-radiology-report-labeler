#heavily modified from https://github.com/chapmanbe/negex/blob/master/negex.python/negex.py

import re

def sortRules(ruleList):
    """Return sorted list of rules.
    
    Rules should be in a tab-delimited format: 'rule\t\t[four letter negation tag]'
    Sorts list of rules descending based on length of the rule, 
    splits each rule into components, converts pattern to regular expression,
    and appends it to the end of the rule. """
    ruleList.sort(key=len, reverse=True)
    sortedList = []
    for rule in ruleList:
        rule = preprocess(rule)
        s = rule.strip().split('\t')
        splitTrig = s[0].split()
        trig = r'\s+'.join(splitTrig)
        pattern = r'\b(' + trig + r')\b'
        s.append(re.compile(pattern, re.IGNORECASE))
        sortedList.append(s)
    return sortedList


def preprocess(input_text):
    result = input_text.lower()
    result = result.replace("ä", "ae")
    result = result.replace("ö", "oe")
    result = result.replace("ü", "ue")
    return result


def get_phrase_type(token):
    if '[PHRASE_POS]' in token:
        return 'POS'
    elif '[PHRASE_NEG]' in token:
        return 'NEG'
    elif '[PHRASE_UNC]' in token:
        return 'UNC'
    else:
        return 'NONE'


class negTagger(object):
    '''Take a sentence and tag negation terms and negated phrases.
    
    Keyword arguments:
    sentence -- string to be tagged
    phrases  -- list of phrases to check for negation
    rules    -- list of negation trigger terms from the sortRules function
    negP     -- tag 'possible' terms as well (default = True)    '''

    def __init__(self, sentence='', phrases=None, rules=None):
        self.__sentence = preprocess(sentence)
        self.__phrases = phrases
        self.__rules = rules
        self.__negTaggedSentence = ''
        self.__scopesToReturn = []
        self.__negationFlag = None
        self.filler = '_'

        for rule in self.__rules:
            reformatRule = re.sub(r'\s+', self.filler, rule[0].strip())
            self.__sentence = rule[3].sub(' ' + rule[2].strip()
                                          + reformatRule
                                          + rule[2].strip() + ' ', self.__sentence)

        positve_phrases, negative_phrases, uncertain_phrases = phrases
        self.tag_phrases(positve_phrases, '[PHRASE_POS]')
        self.tag_phrases(negative_phrases, '[PHRASE_NEG]')
        self.tag_phrases(uncertain_phrases, '[PHRASE_UNC]')

        sentenceTokens = self.__sentence.split()

        negation_range = 15
        self.__negTaggedSentence = ""
        for i in range(len(sentenceTokens)):
            # determine whether current token is any type of known phrase
            phrase_type = get_phrase_type(sentenceTokens[i])
            if phrase_type != "NONE":
                prenFlag = False
                postnFlag = False
                prepFlag = False
                postpFlag = False
                # check for prenegation
                relevant_previous_tokens = sentenceTokens[max(0, i - negation_range):i]
                for token in reversed(relevant_previous_tokens):
                    if "." in token or ";" in token or '[CONJ]' in token:
                        break

                    if '[pren]' in token:
                        prenFlag = True
                        break
                    elif '[prep]' in token:
                        prepFlag = True

                # check for post negation
                if "." in sentenceTokens[i]:
                    relevant_following_tokens = []
                else:
                    relevant_following_tokens = sentenceTokens[i + 1:min(i + 1 + negation_range, len(sentenceTokens))]

                pren_before_postn = False
                for token in relevant_following_tokens:
                    if '[conj]' in token:
                        break
                    if '[pren]' in token:
                        pren_before_postn = True

                    if '[post]' in token:
                        if not pren_before_postn:
                            postnFlag = True
                        break
                    elif '[posp]' in token:
                        postpFlag = True
                        break

                    if "." in token or ";" in token:
                        break

                phrase_is_negated = prenFlag != postnFlag
                if phrase_is_negated:
                    if phrase_type == "POS":
                        sentenceTokens[i] = sentenceTokens[i].replace('[PHRASE_POS]', '[NEGATED]')
                    elif phrase_type == "NEG":
                        sentenceTokens[i] = sentenceTokens[i].replace('[PHRASE_NEG]', '[POSITIVE]')
                    elif phrase_type == "UNC":
                        sentenceTokens[i] = sentenceTokens[i].replace('[PHRASE_UNC]', '[UNCERTAIN]')
                else:
                    if phrase_type == "POS":
                        sentenceTokens[i] = sentenceTokens[i].replace('[PHRASE_POS]', '[POSITIVE]')
                    elif phrase_type == "NEG":
                        sentenceTokens[i] = sentenceTokens[i].replace('[PHRASE_NEG]', '[NEGATED]')
                    elif phrase_type == "UNC":
                        sentenceTokens[i] = sentenceTokens[i].replace('[PHRASE_UNC]', '[UNCERTAIN]')

                if prepFlag or postpFlag:
                    if phrase_type == "POS":
                        sentenceTokens[i] = sentenceTokens[i].replace('[POSITIVE]', '[UNCERTAIN]')
                    elif phrase_type == "NEG":
                        sentenceTokens[i] = sentenceTokens[i].replace('[NEGATED]', '[UNCERTAIN]')

            self.__negTaggedSentence += sentenceTokens[i]
            self.__negTaggedSentence += " "

        if '[POSITIVE]' in self.__negTaggedSentence:
            self.__negationFlag = '1'
        elif '[UNCERTAIN]' in self.__negTaggedSentence:
            self.__negationFlag = '-1'
        elif '[NEGATED]' in self.__negTaggedSentence:
            self.__negationFlag = '0'
        else:
            self.__negationFlag = ''

    def tag_phrases(self, phrases, tag):
        for phrase in phrases:
            phrase = preprocess(phrase)
            splitPhrase = phrase.split()
            joiner = r'\W+'
            joinedPattern = r'\b\w*' + joiner.join(splitPhrase) + r'\w*\b'
            reP = re.compile(joinedPattern, re.IGNORECASE)
            m = reP.search(self.__sentence)
            if m:
                self.__sentence = self.__sentence.replace(m.group(0), tag
                                                          + re.sub(r'\s+', self.filler, m.group(0).strip())
                                                          + tag)

    def getNegTaggedSentence(self):
        return self.__negTaggedSentence

    def getNegationFlag(self):
        return self.__negationFlag

    def getScopes(self):
        return self.__scopesToReturn

    def __str__(self):
        text = self.__negTaggedSentence
        text += '\t' + self.__negationFlag
        text += '\t' + '\t'.join(self.__scopesToReturn)
