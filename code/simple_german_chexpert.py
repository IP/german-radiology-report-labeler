import multiprocessing

import numpy as np
import pandas as pd
from tqdm import tqdm

from negex import negTagger

def detect_label(observation_phrases, report, rules):
    tagger = negTagger(sentence=report, phrases=observation_phrases, rules=rules)
    label = tagger.getNegationFlag()
    return label

def load_observation_phrases(path, observations):
    observation_phrases_dict = {}
    for observation in observations:
        with open(path + "positive/" + observation + ".txt") as file:
            positive_observation_phrases = [line.rstrip() for line in file]
        with open(path + "negative/" + observation + ".txt") as file:
            negative_observation_phrases = [line.rstrip() for line in file]
        with open(path + "uncertain/" + observation + ".txt") as file:
            uncertain_observation_phrases = [line.rstrip() for line in file]
        observation_phrases_dict[observation] = (
        positive_observation_phrases, negative_observation_phrases, uncertain_observation_phrases)

    return observation_phrases_dict


def analyse_report(report, observation_phrases_dict, rules):
    report_dict = {"no_finding": "1"}
    for observation, phrases in observation_phrases_dict.items():
        if observation == "no_finding":
            continue
        label = detect_label(phrases, report, rules)
        report_dict[observation] = label

        if label == "1" or label == "-1":
            if observation != "support_devices":
                report_dict["no_finding"] = "0"
    if report_dict["cardiomegaly"] == "1":
        report_dict["enlarged_cardiomediastinum"] = "1"

    return report_dict


def analyse_df(df, observation_phrases_dict, rules):
    if len(df) == 0:
        return df
    labeling_results = []
    counter = 0
    for index, row in tqdm(df.iterrows()):
        text1 = row['ReportTextDiagnosis']
        text2 = row['ReportTextReport']
        if type(text1) is float and type(text2) is float:
            empty_entry = [""] * len(analysis_result.values())
            labeling_results.append((index, empty_entry))
        else:
            if type(text1) is float:
                text1 = ""
            if type(text2) is float:
                text2 = ""
            text = text1 + text2
            analysis_result = analyse_report(text, observation_phrases_dict, rules)
            labeling_results.append((index, *analysis_result.values()))
        counter += 1

    columns = ["id"] + list(analysis_result.keys())
    result_pandas = pd.DataFrame(labeling_results, columns=columns)
    combined = pd.merge(df, result_pandas, how='right', right_on="id", left_index=True)
    return combined


def analyse_labeled_csv_labeling_tool(df, observation_phrases_dict, rules):
    if len(df) == 0:
        return df
    labeling_results = []
    counter = 0
    for index, row in tqdm(df.iterrows()):
        analysis_result = analyse_report(row["report_text"], observation_phrases_dict, rules)
        labeling_results.append((index, *analysis_result.values()))
        counter += 1

    columns = ["id"] + list(analysis_result.keys())
    result_pandas = pd.DataFrame(labeling_results, columns=columns)
    df = df[["report_index", "report_text"]]
    combined = pd.merge(df, result_pandas, how='right', right_on="id", left_index=True)
    return combined


def analyse_csv_file_multi_threading(path_to_input, result_path, rules, num_threads=12, write=False, phrases=None):
    result = analyse_df_multi_threading(pd.read_csv(path_to_input), rules=rules, num_threads=num_threads,
                                        phrases=phrases)
    if write:
        result.to_csv(result_path, index=True)


def analyse_df_multi_threading(df, rules, num_threads=12, phrases=None):
    split_df = np.array_split(df, num_threads)
    if "level_0" in df.columns:
        df.drop("level_0", axis=1, inplace=True)
    df.reset_index(inplace=True)
    if "id" not in df.columns:
        df.rename(columns={'index': 'id'}, inplace=True)
    if phrases is None:
        observations = ["atelectasis", "cardiomegaly", "consolidation", "edema", "enlarged_cardiomediastinum",
                        "fracture", "lung_lesion", "lung_opacity", "no_finding", "pleural_effusion", "pleural_other",
                        "pneumonia", "pneumothorax", "support_devices"]
        phrases = load_observation_phrases("../phrases_and_triggers/phrases/", observations)

    from functools import partial
    fun = partial(analyse_labeled_csv_labeling_tool, observation_phrases_dict=phrases, rules=rules)

    with multiprocessing.Pool(num_threads) as p:
        results = p.map(fun, split_df)

    concatenated_df = pd.concat(results)
    cols = concatenated_df.columns.tolist()
    index_id_column = cols.index("id")
    cols = [cols[index_id_column]] + cols[:index_id_column] + cols[index_id_column + 1:]
    concatenated_df = concatenated_df[cols]
    concatenated_df.sort_values('id', inplace=True)
    return concatenated_df
