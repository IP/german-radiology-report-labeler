Herz Mediastinum mittelständig
Herz und Mediastinum idem
Herz und Mediastinum in unveränderter Projektion
Herz und Mediastinum mittelständig
Herz und Mediastinum projizieren sich unverändert
Herz und Mediastinum unverändert konfiguriert
Herz und oberes Mediastinum unverändert
Herz unverändert
Herz unverändert konfiguriert
Herz- und Mediastinalkonfiguration idem
Herz- und Mediastinalkonfiguration im Liegen unverändert
Herz- und Mediastinalkonfiguration ohne richtungsweisende Befundänderung
Herz- und Mediastinalkonfiguration projizieren sich im Wesentlichen unverändert
Herz- und Mediastinalkonfiguration unverändert
Herz- und Mediastinalkonfiguration zur Voruntersuchung unverändert
Herzkonfiguration idem
Herzschatten grenzwertig weit
Herzschatten mittelständig
Herzschatten und oberes Mediastinum unverändert
Herzsilhouette unverändert
Im Liegen deutlich verbreitertes oberes Mediastinum sowie Herzschatten
Im Liegen verbreitertes Herz
Konfiguration von Herz und Mediastinum unverändert
Mittelständige Projektion des oberen Mediastinums und Herzkontur
Unveränderte Herz
Unveränderte Konfiguration des Herz
Unveränderte Konfiguration von Herz
Unveränderte Konfiguration von oberem Mediastinum und Herzschatten
Unveränderte Mediastinal- und Herzkonfiguration
Unveränderter Herz- und Mediastinalkonfiguration
Verbreiterte Herzgröße im Liegen
im Liegen verbreitert
unveränderte Herz- und Mediastinalkonfiguration
unveränderte Konfiguration von Mediastinum und Herz
